package de.dersnatch.lost.enums;

public enum LostItemSet {

    SCHATTENKRIEGER("Schattenkriger", RarityType.EPIC),
    WALLWACHE("Wallwache", RarityType.LEGENDARY);

    private String name;
    private RarityType rarityType;
    private LostEffekt[] lostEffekts;

    LostItemSet(String name, RarityType rarityType, LostEffekt... lostEffekts) {
        this.name = name;
        this.rarityType = rarityType;
        this.lostEffekts = lostEffekts;
    }

    public LostEffekt[] getLostEffekts() {
        return lostEffekts;
    }

    public String getName() {
        return name;
    }

    public RarityType getRarityType() {
        return rarityType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRarityType(RarityType rarityType) {
        this.rarityType = rarityType;
    }
}
