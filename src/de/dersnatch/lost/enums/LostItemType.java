package de.dersnatch.lost.enums;

public enum LostItemType {

    WEAPON("Waffe"),
    BOW("Bogen"),
    ARMOR("Rüstungsteil"),
    TOOL("Werkzeug"),
    PROJECTILE("Köcher"),
    OTHER("Sonstiges");

    private String name;

    LostItemType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
