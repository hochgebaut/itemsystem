package de.dersnatch.lost.enums;

import org.bukkit.Material;

public enum Item {

    //@Tools
    HOLZAXT("Holzbeil", Material.WOOD_AXE, RarityType.USUALLY, LostItemType.TOOL, 60, 60, 4, 4, 1.5, 0.0),
    STEINAXT("Steinbeil", Material.STONE_AXE, RarityType.USUALLY, LostItemType.TOOL, 60, 60, 4, 4, 1.5, 0.0),
    GOLDAXT("Goldbeil", Material.GOLD_AXE, RarityType.USUALLY, LostItemType.TOOL, 60, 60, 4, 4, 1.5, 0.0),
    EISENAXT("Eisenbeil", Material.IRON_AXE, RarityType.USUALLY, LostItemType.TOOL, 60, 60, 4, 4, 1.5, 0.0),
    DIAMANTAXT("Diamantbeil", Material.DIAMOND_AXE, RarityType.USUALLY, LostItemType.TOOL, 60, 60, 4, 4, 1.5, 0.0),

    HOLZSPITZHACKE("Holzspitzhacke", Material.WOOD_PICKAXE, RarityType.USUALLY, LostItemType.TOOL, 60, 60, 4, 4, 1.5, 0.0),
    STEINSPITZHACKE("Steinspitzhacke", Material.STONE_PICKAXE, RarityType.USUALLY, LostItemType.TOOL, 60, 60, 4, 4, 1.5, 0.0),
    GOLDSPITZHACKE("Goldspitzhacke", Material.GOLD_PICKAXE, RarityType.USUALLY, LostItemType.TOOL, 60, 60, 4, 4, 1.5, 0.0),
    EISENSPITZHACKE("Eisenspitzhacke", Material.IRON_PICKAXE, RarityType.USUALLY, LostItemType.TOOL, 60, 60, 4, 4, 1.5, 0.0),
    DIAMANTSPITZHACKE("Diamantspitzhacke", Material.DIAMOND_PICKAXE, RarityType.USUALLY, LostItemType.TOOL, 60, 60, 4, 4, 1.5, 0.0, new LostEffekt(EffektType.GLÜCKSPILZ, 3)),

    HOLZSCHAUFEL("Holzspaten", Material.WOOD_SPADE, RarityType.USUALLY, LostItemType.TOOL, 60, 60, 4, 4, 1.5, 0.0),
    STEINSCHAUFEL("Steinspaten", Material.STONE_SPADE, RarityType.USUALLY, LostItemType.TOOL, 60, 60, 4, 4, 1.5, 0.0),
    GOLDSCHAUFEL("Goldspaten", Material.GOLD_SPADE, RarityType.USUALLY, LostItemType.TOOL, 60, 60, 4, 4, 1.5, 0.0),
    EISENSCHAUFEL("Eisenspaten", Material.IRON_SPADE, RarityType.USUALLY, LostItemType.TOOL, 60, 60, 4, 4, 1.5, 0.0),
    DIAMANTSCHAUFEL("Diamantschaufel", Material.DIAMOND_SPADE, RarityType.USUALLY, LostItemType.TOOL, 60, 60, 4, 4, 1.5, 0.0),

    HOLZHACKE("Holzsense", Material.DIAMOND_HOE, RarityType.USUALLY, LostItemType.TOOL, 60, 60, 4, 4, 1.5, 0.0),
    STEINHACKE("Steinsense", Material.DIAMOND_HOE, RarityType.USUALLY, LostItemType.TOOL, 60, 60, 4, 4, 1.5, 0.0),
    GOLDHACKE("Goldsense", Material.DIAMOND_HOE, RarityType.USUALLY, LostItemType.TOOL, 60, 60, 4, 4, 1.5, 0.0),
    EISENHACKE("Eisensense", Material.DIAMOND_HOE, RarityType.USUALLY, LostItemType.TOOL, 60, 60, 4, 4, 1.5, 0.0),
    DIAMANTHACKE("Diamantsense", Material.DIAMOND_HOE, RarityType.USUALLY, LostItemType.TOOL, 60, 60, 4, 4, 1.5, 0.0, new LostEffekt(EffektType.REPARATUR)),

    //@Weapons
    HOLZKLINGE("Holzklinge", Material.WOOD_SWORD, RarityType.USUALLY, LostItemType.WEAPON, 60, 60, 4, 4, 1.5, 0.0),
    STEINKLINGE("Steinklinge", Material.STONE_SWORD, RarityType.USUALLY, LostItemType.WEAPON, 60, 60, 4, 4, 1.5, 0.0),
    GOLDKLINGE("Goldklinge", Material.GOLD_SWORD, RarityType.USUALLY, LostItemType.WEAPON, 60, 60, 4, 4, 1.5, 0.0),
    EISENKLINGE("Eisenklinge", Material.IRON_SWORD, RarityType.USUALLY, LostItemType.WEAPON, 60, 60, 4, 4, 1.5, 0.0),
    DIAMANTKLINGE("Diamantklinge", Material.DIAMOND_SWORD, RarityType.USUALLY, LostItemType.WEAPON, 60, 60, 4, 4, 1.5, 0.0, new LostEffekt(EffektType.REPARATUR)),

    GEHSTOCK("Gehstock", Material.STICK, RarityType.MAGIC, LostItemType.WEAPON, 500, 1000, 5, 6, 2.5, 0.1),
    SCHÄDELSPIKER("Schädelspiker", Material.IRON_SWORD, RarityType.MAGIC, LostItemType.WEAPON, 500, 1000, 5, 6, 2.5, 0.1),
    SCHWERER_AST("Schwerer Ast", Material.STICK, RarityType.MAGIC, LostItemType.WEAPON, 500, 1000, 5, 6, 2.5, 0.1),
    GROBE_KRIEGSAXT("Grobe Kriegsaxt", Material.IRON_AXE, RarityType.MAGIC, LostItemType.WEAPON, 500, 1000, 5, 6, 2.5, 0.1),
    KRUMMSCHWERT("Krummschwert", Material.IRON_SWORD, RarityType.MAGIC, LostItemType.WEAPON, 500, 1000, 5, 6, 2.5, 0.1),
    HOLZFÄLLERAXT("Holzfälleraxt", Material.IRON_AXE, RarityType.MAGIC, LostItemType.WEAPON, 500, 1000, 5, 6, 2.5, 0.1),
    VERROSTETES_HAUMESSER("Verrostetes Haumesser", Material.IRON_SWORD, RarityType.MAGIC, LostItemType.WEAPON, 500, 1000, 5, 6, 2.5, 0.1),

    JAGDMESSER("Jagdmesser", Material.IRON_SWORD, RarityType.RARE, LostItemType.WEAPON, 500, 1000, 6, 8, 2.0, 0.2),
    MACHETE("Machete", Material.IRON_SWORD, RarityType.RARE, LostItemType.WEAPON, 500, 1000, 6, 8, 2.0, 0.2),
    KLEINE_SENSE("Kleine Sense", Material.IRON_SPADE, RarityType.RARE, LostItemType.WEAPON, 500, 1000, 6, 8, 2.0, 0.2),
    KURZSCHWERT("Kurzschwert", Material.IRON_SWORD, RarityType.RARE, LostItemType.WEAPON, 500, 1000, 6, 8, 2.0, 0.2),

    GROBES_KRIEGSBEIL("Grobes Kriegsbeil", Material.IRON_SWORD, RarityType.UNCOMMON, LostItemType.WEAPON, 500, 1000, 6, 10, 2.0, 0.3),
    EDLES_KURZSCHWERT("Edles Kurzschwert", Material.IRON_SWORD, RarityType.UNCOMMON, LostItemType.WEAPON, 500, 1000, 6, 10, 2.0, 0.3),
    WURFMESSER("Wurfmesser", Material.IRON_SWORD, RarityType.UNCOMMON, LostItemType.WEAPON, 500, 1000, 6, 10, 2.0, 0.3),
    HELLEBARDE("Hellebarde", Material.IRON_SWORD, RarityType.UNCOMMON, LostItemType.WEAPON, 500, 1000, 6, 10, 2.0, 0.3),

    DRESCHFLEGEL("Dreschflegel", Material.IRON_SWORD, RarityType.EPIC, LostItemType.WEAPON, 500, 1000, 6, 12, 2.0, 0.4),
    SCHLACHTMESSER("Schlachtmesser", Material.IRON_SWORD, RarityType.EPIC, LostItemType.WEAPON, 500, 1000, 6, 12, 2.0, 0.4, new LostEffekt(EffektType.SOULBOUND)),

    ASSASINENSCHWERT("Assasinenschwert", Material.DIAMOND_SWORD, RarityType.MYSTIC, LostItemType.WEAPON, 500, 1000, 9, 15, 2.0, 0.5),

    ZORN_DES_DÄMONS("Zorn des Dämons", Material.DIAMOND_SWORD, RarityType.LEGENDARY, LostItemType.WEAPON, 500, 1000, 10, 20, 2.0, 0.5, new LostEffekt(EffektType.REPARATUR)),
    HEILIGER_VOLLSTRECKER("Heiliger Vollstrecker", Material.DIAMOND_SWORD, RarityType.LEGENDARY, LostItemType.WEAPON, 500, 1000, 10, 20, 2.0, 0.6),

    LEICHENMACHER("Leichenmacher", Material.DIAMOND_SWORD, RarityType.PHENOMENAL, LostItemType.WEAPON, 2000, 5000, 15, 40, 2.0, 0.7),
    DEGEN_DER_BANDITEN("Degen der Banditen", Material.DIAMOND_SWORD, RarityType.PHENOMENAL, LostItemType.WEAPON, 2000, 5000, 15, 45, 2.0, 0.7, new LostEffekt(EffektType.LEBENSRAUB, 2)),
    STAB_DES_SCHWARZMAGIERS("Stab des Schwarzmagiers", Material.DIAMOND_SWORD, RarityType.PHENOMENAL, LostItemType.WEAPON, 2000, 5000, 15, 50, 2.0, 0.7),
    STAB_DES_FEUERMAGIERS("Stab des Feuermagiers", Material.DIAMOND_SWORD, RarityType.PHENOMENAL, LostItemType.WEAPON, 2000, 5000, 15, 55, 2.0, 0.7),
    SCHWERT_DER_LEGENDEN("Schwert der Untoten", Material.DIAMOND_SWORD, RarityType.PHENOMENAL, LostItemType.WEAPON, 2000, 5000, 15, 60, 2.0, 0.7, new LostEffekt(EffektType.WITHER, 20*2, 2)),

    //@Bows
    SURRENDER_TODESBOTE("Surrender Todesbote", Material.BOW, RarityType.RARE, LostItemType.BOW, 750, 1250, 8, 13, 0.15),

    //@Armor
    HAUBE_DES_SCHATTENKRIEGERS("Haube des Schattenkriegers", Material.DIAMOND_HELMET, LostItemType.ARMOR, 2500, 5000, 6, 10, 0.9, LostItemSet.SCHATTENKRIEGER),

    BRUST_DER_ZERBERSTUNG("Brust der Zerberstung", Material.DIAMOND_CHESTPLATE, LostItemType.ARMOR, 3000, 6000, 6, 10, 1.3, LostItemSet.WALLWACHE, new LostEffekt(EffektType.LEBENSPUNKTE, 5)),
    HAUBE_DER_ZERBERSTUNG("Haube der Zerberstung", Material.DIAMOND_HELMET, LostItemType.ARMOR, 3000, 6000, 6, 10, 1.3, LostItemSet.WALLWACHE),
    HOSE_DER_ZERBERSTUNG("Hose der Zerberstung", Material.DIAMOND_LEGGINGS, LostItemType.ARMOR, 3000, 6000, 6, 10, 1.3, LostItemSet.WALLWACHE),
    BOOTS_DER_ZERBERSTUNG("Stiefel der Zerberstung", Material.DIAMOND_BOOTS, LostItemType.ARMOR, 3000, 6000, 6, 10, 1.3, LostItemSet.WALLWACHE, new LostEffekt(EffektType.SOULBOUND));

    //@General
    private String name;
    private Material material;
    private RarityType rarityType;
    private LostItemType itemType;
    private int minDurability;
    private int maxDurability;
    private double weight;
    private LostEffekt[] lostEffekts;

    //@Weapons
    private double attackSpeed;

    //@Weapons & Bows
    private int minAttackDamage;
    private int maxAttackDamage;

    //@Armor
    private int minProtection;
    private int maxProtection;
    private LostItemSet itemSet;

    //@Weapons
    Item(String name, Material material, RarityType rarityType, LostItemType itemType, int minDurability, int maxDurability, int minAttackDamage, int maxAttackDamage, double attackSpeed, double weight, LostEffekt... lostEffekts) {
        this.name = name;
        this.material = material;
        this.rarityType = rarityType;
        this.itemType = itemType;
        this.minDurability = minDurability;
        this.maxDurability = maxDurability;
        this.minAttackDamage = minAttackDamage;
        this.maxAttackDamage = maxAttackDamage;
        this.attackSpeed = attackSpeed;
        this.weight = weight;
        this.lostEffekts = lostEffekts;
    }

    //@Bows
    Item(String name, Material material, RarityType rarityType, LostItemType itemType, int minDurability, int maxDurability, int minAttackDamage, int maxAttackDamage, double weight, LostEffekt... lostEffekts) {
        this.name = name;
        this.material = material;
        this.rarityType = rarityType;
        this.itemType = itemType;
        this.minDurability = minDurability;
        this.maxDurability = maxDurability;
        this.minAttackDamage = minAttackDamage;
        this.maxAttackDamage = maxAttackDamage;
        this.weight = weight;
        this.lostEffekts = lostEffekts;
    }

    //@Armor
    Item(String name, Material material, LostItemType itemType, int minDurability, int maxDurability, int minProtection, int maxProtection, double weight, LostItemSet itemSet, LostEffekt... lostEffekts) {
        this.name = name;
        this.material = material;
        this.rarityType = itemSet.getRarityType();
        this.itemType = itemType;
        this.minDurability = minDurability;
        this.maxDurability = maxDurability;
        this.minProtection = minProtection;
        this.maxProtection = maxProtection;
        this.weight = weight;
        this.itemSet = itemSet;
        this.lostEffekts = lostEffekts;
    }

    public LostEffekt[] getLostEffekts() {
        return lostEffekts;
    }

    public RarityType getRarityType() {
        return rarityType;
    }

    public String getName() {
        return name;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public int getMinDurability() {
        return minDurability;
    }

    public double getWeight() {
        return weight;
    }

    public int getMaxDurability() {
        return maxDurability;
    }

    public LostItemType getItemType() {
        return itemType;
    }

    public int getMaxAttackDamage() {
        return maxAttackDamage;
    }

    public Material getMaterial() {
        return material;
    }

    public int getMinAttackDamage() {
        return minAttackDamage;
    }

    public int getMaxProtection() {
        return maxProtection;
    }

    public int getMinProtection() {
        return minProtection;
    }

    public LostItemSet getItemSet() {
        return itemSet;
    }
}
