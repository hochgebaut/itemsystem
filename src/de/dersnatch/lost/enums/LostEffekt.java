package de.dersnatch.lost.enums;

public class LostEffekt {

    private EffektType effektType;
    private int lenght;
    private int strenght;
    private int amount;

    public LostEffekt(EffektType effektType, int lenght, int strenght) {
        this.effektType = effektType;
        this.lenght = lenght;
        this.strenght = strenght;
    }

    public LostEffekt(EffektType effektType, int amount) {
        this.effektType = effektType;
        this.amount = amount;
    }

    public LostEffekt(EffektType effektType) {
        this.effektType = effektType;
    }

    public int getAmount() {
        return amount;
    }

    public int getLenght() {
        return lenght;
    }

    public int getStrenght() {
        return strenght;
    }

    public EffektType getEffektType() {
        return effektType;
    }
}
