package de.dersnatch.lost.enums;

import org.bukkit.ChatColor;

public enum RarityType {

    USUALLY("Gewöhnlich", ChatColor.GRAY, (byte) 7),
    MAGIC("Magisch", ChatColor.BLUE, (byte) 8),
    RARE("Selten", ChatColor.DARK_BLUE, (byte) 12),
    UNCOMMON("Außergewöhnlich", ChatColor.AQUA, (byte) 6),
    EPIC("Episch", ChatColor.LIGHT_PURPLE, (byte) 13),
    MYSTIC("Mystisch", ChatColor.DARK_PURPLE, (byte) 5),
    LEGENDARY("Legendär", ChatColor.GOLD, (byte) 14),
    PHENOMENAL("Phänomenal", ChatColor.RED, (byte) 1);

    private String name;
    private ChatColor color;
    private byte cc;

    RarityType(String name, ChatColor color, byte cc) {
        this.name = name;
        this.color = color;
        this.cc = cc;
    }

    public String getName() {
        return name;
    }

    public ChatColor getColor() {
        return color;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setColor(ChatColor color) {
        this.color = color;
    }

    public byte getCc() {
        return cc;
    }
}
