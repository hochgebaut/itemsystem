package de.dersnatch.lost.enums;

public enum EffektType {

    WITHER("§8Wither"),
    GIFT("§2Gift"),
    LEBENSPUNKTE("Lebenspunkte"),
    LEBENSRAUB("Lebensraub"),
    SOULBOUND("Soulbound"),
    TREFFSICHER("Treffsicher"),
    GLÜCKSPILZ("Glückspilz"),
    HUNGERSNOT("Hungersnot"),
    REPARATUR("Reparatur");


    private String name;

    EffektType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
