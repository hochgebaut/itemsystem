package de.dersnatch.lost.villager;

import de.dersnatch.lost.LostItemSystem;
import org.bukkit.Location;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class VillagerListener implements Listener {

    @EventHandler
    public void on(PlayerInteractEntityEvent event) {
        Player player = event.getPlayer();
        if (event.getRightClicked().hasMetadata("MobType")) {
            event.setCancelled(true);
            player.sendMessage("§eTest :)");
            player.sendMessage("§eTest :)");
            VillagerInventory inventory = new VillagerInventory();
            inventory.openInventory(player);
        }
    }

    public static ArmorStand spawnMob(Location loc) {
        Entity entity = loc.getWorld().spawnEntity(loc, EntityType.VILLAGER);
        entity.setCustomName("§7[§6Schmied§7] §fWaffenschmied");
        entity.setCustomNameVisible(true);
        Villager villager = (Villager) entity;
        villager.setProfession(Villager.Profession.BLACKSMITH);
        ArmorStand holder = (ArmorStand) loc.getWorld().spawnEntity(loc, EntityType.ARMOR_STAND);
        holder.setGravity(false);
        holder.setVisible(false);
        holder.setNoDamageTicks(20 * 60 * 90);
        holder.setPassenger(villager);
        villager.setMetadata("MobType", new FixedMetadataValue(LostItemSystem.getInstance(), "RepairVillager"));
        holder.setMetadata("MobType", new FixedMetadataValue(LostItemSystem.getInstance(), "RepairVillager"));
        return holder;
    }

}
