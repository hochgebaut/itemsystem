package de.dersnatch.lost.villager;

import de.dersnatch.lost.LostItemSystem;
import de.dersnatch.lost.enums.Item;
import de.dersnatch.lost.enums.LostItemType;
import de.dersnatch.lost.enums.RarityType;
import de.dersnatch.lost.manager.ItemManager;
import de.dersnatch.lost.manager.LostItem;
import de.dersnatch.lost.util.LostItemBuilderAPI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.HashMap;

public class VillagerInventory implements Listener {

    private Inventory inventory;
    private Inventory repairInventory;
    private Inventory dismantInventory;
    private Inventory craftInventory;
    private Inventory revalueInventory;
    private Inventory craftInventoryFinal;
    private Inventory raritySelectInventory;
    private RarityType rarityTypee = null;
    private RarityType dismantRarity = null;

    private HashMap<Player, Integer> clickedItems = new HashMap<>();
    private HashMap<Player, Integer> clickedDismantItems = new HashMap<>();
    private HashMap<Player, Integer> clickedValItems = new HashMap<>();

    private ItemStack placeHolder = new LostItemBuilderAPI(Material.STAINED_GLASS_PANE, (byte) 8).name("§7<~>").getItemStack();
    private ItemStack resultItem;
    private ItemStack itemToRepair;
    private ItemStack selHelmet;
    private ItemStack selChest;
    private ItemStack selLeggins;
    private ItemStack selBoots;
    private ItemStack selSword;
    private ItemStack selBow;
    private ItemStack dismantPowder;
    private ItemStack itemToReVal;
    private InventoryClickEvent event;
    private ItemStack repairItem = new LostItemBuilderAPI(Material.SKULL_ITEM)
            .name("§7» Items reparieren")
            .lore(Arrays.asList("", "§fHier kannst du deine kaputten Items", "§fwieder reparieren!"))
            .setTexture("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOWI0MjVhYTNkOTQ2MThhODdkYWM5Yzk0ZjM3N2FmNmNhNDk4NGMwNzU3OTY3NGZhZDkxN2Y2MDJiN2JmMjM1In19fQ==")
            .getItemStack();
    private ItemStack dismantleItem = new LostItemBuilderAPI(Material.SKULL_ITEM)
            .name("§7» Items zerlegen")
            .lore(Arrays.asList("", "§fHier kannst du deine Items, welche du nicht benötigst", "§ffür wertvollen Staub zerlegen lassen!"))
            .setTexture("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNzkxODZiY2M2NzIwNzBmOWQ1N2MxZjJlYzFlYjk4ZWFmZTM5M2U5NGRkMjFlZWE0ZWM4ODdhN2RlMjk1ZjFjMCJ9fX0=")
            .getItemStack();
    private ItemStack craftItem = new LostItemBuilderAPI(Material.SKULL_ITEM)
            .name("§7» Items herstellen")
            .lore(Arrays.asList("", "§fHier kannst du mit Hilfe von zerlegtem Staub", "dir Items jeder Seltenheitsstufe craften!"))
            .setTexture("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTg3OWU2NGZkMmUyZDE1ZDFiN2U0N2FlNDBmYTUyOGZjNzIwODZhMzEyZDZhNmVkM2Y3ZTU1MmFmOWQ1In19fQ==")
            .getItemStack();
    private ItemStack revalueItem = new LostItemBuilderAPI(Material.SKULL_ITEM)
            .name("§7» Items aufbessern")
            .lore(Arrays.asList("", "§fHier kannst du deine Items für etwas Ressourcen", "§fbis zu 3 mal aufbessern!", "§fHierdurch bekommt dein Item bessere Randomwerte!"))
            .setTexture("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNWE4ZWY5MmVmYTE1NjY5ZGY0ODMzMmQxMThhMmY3MDU3NzJhMmEzZmNkMGZiODJhNjFmMjc3Yjg0OWEyYmQ2In19fQ==")
            .getItemStack();
    private Player player;
    private Listener listener;

    public VillagerInventory() {
        listener = new Listener() {
            public InventoryClickEvent event;

            @EventHandler
            public void onClick(InventoryClickEvent event) {
                this.event = event;
                Player player = (Player) event.getWhoClicked();
                if (event.getClickedInventory() != null) {
                    if (event.getClickedInventory().equals(inventory)) {
                        if (!event.getCurrentItem().equals(placeHolder)) {
                            if (event.getCurrentItem().equals(repairItem)) {
                                event.setCancelled(true);
                                openRapairInventory();
                                return;
                            }
                            if (event.getCurrentItem().equals(dismantleItem)) {
                                event.setCancelled(true);
                                openDismantleInventory();
                                return;
                            }
                            if (event.getCurrentItem().equals(craftItem)) {
                                event.setCancelled(true);
                                player.sendMessage("§c§LDieses Feature ist derzeit nur eingeschränkt verfügbar!");
                                if (rarityTypee != null) {
                                    openCraftInventory();
                                } else {
                                    openSelectRarityInventory();
                                }
                            }
                            if (event.getCurrentItem().equals(revalueItem)) {
                                event.setCancelled(true);
                                openRevalueInventory();
                                return;
                            }
                            event.setCancelled(true);
                            return;
                        }
                        event.setCancelled(true);
                        return;
                    }
                    if (event.getClickedInventory().equals(raritySelectInventory)) {
                        if (!event.getCurrentItem().equals(placeHolder)) {
                            RarityType rarity;
                            try {
                                event.setCancelled(true);
                                rarity = RarityType.valueOf(ItemManager.getNBTData(event.getCurrentItem(), "RaritySelector"));
                                rarityTypee = rarity;
                                openCraftInventory();
                            } catch (Exception e) {
                                event.setCancelled(true);
                                return;
                            }
                        } else {
                            event.setCancelled(true);
                            return;
                        }
                    }
                    if (event.getClickedInventory().equals(craftInventory)) {
                        if (!event.getCurrentItem().equals(placeHolder)) {
                            event.setCancelled(true);
                            if (event.getCurrentItem().equals(selHelmet))
                                openCraftFinalInventory(selHelmet);
                            if (event.getCurrentItem().equals(selChest))
                                openCraftFinalInventory(selChest);
                            if (event.getCurrentItem().equals(selLeggins))
                                openCraftFinalInventory(selLeggins);
                            if (event.getCurrentItem().equals(selBoots))
                                openCraftFinalInventory(selBoots);
                            if (event.getCurrentItem().equals(selSword))
                                openCraftFinalInventory(selSword);
                            if (event.getCurrentItem().equals(selBow))
                                openCraftFinalInventory(selBow);
                            else
                                event.setCancelled(true);
                        } else {
                            event.setCancelled(true);
                            return;
                        }
                    }

                    /**
                     * Items zerlegen!
                     */
                    if (event.getInventory().equals(dismantInventory)) {
                        if (event.getClickedInventory().equals(dismantInventory)) {
                            if (event.getCurrentItem().equals(dismantPowder)) {
                                int i = clickedDismantItems.get(player);
                                player.getInventory().setItem(i, dismantPowder);
                                dismantPowder = null;
                                player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, SoundCategory.MASTER, 1F, 1F);
                                player.closeInventory();
                                clickedDismantItems.remove(player);
                                openDismantleInventory();
                            } else {
                                event.setCancelled(true);
                            }
                        } else {
                            if (ItemManager.isCustomItem(event.getCurrentItem())) {
                                Item item = Item.valueOf(ItemManager.getNBTData(event.getCurrentItem(), "UUID"));
                                if (!item.getRarityType().equals(RarityType.USUALLY)) {
                                    updateDismantleInventory();
                                } else {
                                    event.setCancelled(true);
                                    player.sendMessage("§cGewöhnliche Items akzeptiere ich nicht ;)");
                                }
                            } else {
                                event.setCancelled(true);
                                player.sendMessage("§cDieses Item akzeptiere ich nicht!");
                            }
                        }
                    }

                    /**
                     * Items reparieren!
                     */
                    if (event.getInventory().equals(repairInventory)) {
                        if (event.getClickedInventory().equals(repairInventory)) {
                            if (event.getCurrentItem().equals(resultItem)) {
                                int i = clickedItems.get(player);
                                player.getInventory().setItem(i, resultItem);
                                resultItem = null;
                                player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, SoundCategory.MASTER, 1F, 1F);
                                player.closeInventory();
                                clickedItems.remove(player);
                                openRapairInventory();
                            } else {
                                event.setCancelled(true);
                            }
                        } else {
                            if (ItemManager.isCustomItem(event.getCurrentItem())) {
                                if (ItemManager.getCurrentDurability(event.getCurrentItem()) != ItemManager.getMaxDurability(event.getCurrentItem())) {
                                    updateRepairInventory();
                                } else {
                                    event.setCancelled(true);
                                    player.sendMessage("§cDieses Item habe ich doch repariert...!");
                                }
                            } else {
                                event.setCancelled(true);
                                player.sendMessage("§cDieses Item werde ich nicht reparieren!");
                            }
                        }
                    }


                    /**
                     * Items aufbessern!
                     */
                    if (event.getInventory().equals(revalueInventory)) {
                        if (event.getClickedInventory().equals(revalueInventory)) {
                            if (event.getCurrentItem().equals(itemToReVal)) {
                                int i = clickedValItems.get(player);
                                player.getInventory().setItem(i, itemToReVal);
                                itemToReVal = null;
                                player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, SoundCategory.MASTER, 1F, 1F);
                                player.closeInventory();
                                clickedValItems.remove(player);
                                openRevalueInventory();
                            } else {
                                event.setCancelled(true);
                            }
                        } else {
                            if (ItemManager.isCustomItem(event.getCurrentItem())) {
                                updateReValInventory();
                            } else {
                                event.setCancelled(true);
                                player.sendMessage("§cDieses Item werde ich nicht aufwerten!");
                            }
                        }
                    }
                }
            }

            private void updateReValInventory() {
                clickedValItems.remove(player);
                if (ItemManager.getCurrentReValueLvl(event.getCurrentItem()) >= 3) {
                    event.setCancelled(true);
                    player.sendMessage("Ich habe dir dieses Item schon 3 mal verbessert...!");
                    return;
                }
                event.setCancelled(true);
                clickedValItems.put(player, event.getSlot());
                ItemStack rechnung = new LostItemBuilderAPI(Material.PAPER).name("§7Rechnungsbeleg").lore(Arrays.asList("§f---------------------", "§f1x Reperatur von " + event.getCurrentItem().getItemMeta().getDisplayName())).getItemStack();
                net.minecraft.server.v1_12_R1.ItemStack copy = CraftItemStack.asNMSCopy(event.getCurrentItem());
                itemToReVal = CraftItemStack.asBukkitCopy(copy);
                revalueInventory.setItem(11, itemToReVal);
                revalueInventory.setItem(4, rechnung);
                itemToReVal = ItemManager.setRevalueLvl(itemToReVal, 1);
                itemToReVal = ItemManager.setAttackDamage(itemToReVal, 1);
                itemToReVal = ItemManager.setAttackSpeed(itemToReVal, 0.1);
                revalueInventory.setItem(15, itemToReVal);
            }

            private void updateDismantleInventory() {
                event.setCancelled(true);
                if (!clickedDismantItems.containsKey(player))
                    clickedDismantItems.put(player, event.getSlot());
                else {
                    clickedDismantItems.remove(player);
                    clickedDismantItems.put(player, event.getSlot());
                }

                Item item = Item.valueOf(ItemManager.getNBTData(event.getCurrentItem(), "UUID"));
                int amount = 0;
                if (item.getItemType().equals(LostItemType.WEAPON))
                    amount = 2;
                if (item.getItemType().equals(LostItemType.BOW))
                    amount = 1;
                if (item.getItemType().equals(LostItemType.ARMOR))
                    amount = 3;

                ItemStack rechnung = new LostItemBuilderAPI(Material.PAPER).name("§7Rechnungsbeleg").lore(Arrays.asList("§f---------------------", "§f1x Zerlegen von " + event.getCurrentItem().getItemMeta().getDisplayName(), "", "§aDu bekommst: §7" + amount + "x " + item.getRarityType().getColor() + item.getRarityType().getName() + "-Staub")).getItemStack();

                dismantRarity = item.getRarityType();

                dismantInventory.setItem(11, event.getCurrentItem());
                dismantInventory.setItem(4, rechnung);

                dismantPowder = null;
                dismantPowder = new LostItemBuilderAPI(Material.INK_SACK, amount, item.getRarityType().getCc()).name("§7Item zerlegen").lore(Arrays.asList("", "§cDas zerlegen kann nicht rückgängig gemacht werden!")).getItemStack();
                dismantInventory.setItem(15, dismantPowder);
            }

            private void updateRepairInventory() {
                clickedItems.remove(player);
                event.setCancelled(true);
                itemToRepair = event.getCurrentItem();
                clickedItems.put(player, event.getSlot());
                int currentDurability = ItemManager.getCurrentDurability(itemToRepair);
                int maxDurability = ItemManager.getMaxDurability(itemToRepair);
                int durabilityToRegenerate = maxDurability - currentDurability;
                ItemStack rechnung = new LostItemBuilderAPI(Material.PAPER).name("§7Rechnungsbeleg").lore(Arrays.asList("§f---------------------", "§f1x Reperatur von " + event.getCurrentItem().getItemMeta().getDisplayName(), "§fKosten: §7" + durabilityToRegenerate / 4 + " Gold")).getItemStack();
                net.minecraft.server.v1_12_R1.ItemStack copy = CraftItemStack.asNMSCopy(itemToRepair);
                resultItem = CraftItemStack.asBukkitCopy(copy);
                repairInventory.setItem(11, itemToRepair);
                repairInventory.setItem(4, rechnung);
                ItemManager.setDurability(resultItem, durabilityToRegenerate);
                repairInventory.setItem(15, resultItem);
            }
        };
        Bukkit.getPluginManager().registerEvents(listener, LostItemSystem.getInstance());
    }

    private void openRevalueInventory() {
        ItemStack helpItem = new LostItemBuilderAPI(Material.SKULL_ITEM)
                .name("§c§LWie funktioniert das aufbessern von Items?")
                .lore(Arrays.asList("§71.» Item welches aufgebessert werden soll auswählen", "§72.» Die Vergütung kann im Feld", "      §f\"Deine Rechnung§7\" eingesehen werden", "§73.» Bestätige das Aufbessern mit einem Klick", "§7      auf \"§fAufwerten lassen§7\""))
                .setTexture("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMmUzZjUwYmE2MmNiZGEzZWNmNTQ3OWI2MmZlZGViZDYxZDc2NTg5NzcxY2MxOTI4NmJmMjc0NWNkNzFlNDdjNiJ9fX0=")
                .getItemStack();
        ItemStack insertItemHere = new LostItemBuilderAPI(Material.BARRIER).name("§cBitte wähle ein Item aus!").getItemStack();
        ItemStack resultItem = new LostItemBuilderAPI(Material.BARRIER).name("§cKein Item ausgewählt!").getItemStack();
        this.revalueInventory = Bukkit.createInventory(this.player, 27, "§fItem aufwerten lassen");
        this.revalueInventory.setItem(11, insertItemHere);
        this.revalueInventory.setItem(15, resultItem);
        this.revalueInventory.setItem(22, helpItem);
        for (int i = 0; i < this.revalueInventory.getSize(); i++) {
            if (this.revalueInventory.getItem(i) == null) {
                this.revalueInventory.setItem(i, this.placeHolder);
            }
        }
        this.player.openInventory(this.revalueInventory);
    }

    private void openDismantleInventory() {
        ItemStack helpItem = new LostItemBuilderAPI(Material.SKULL_ITEM)
                .name("§c§LWie funktioniert das zerlegen in Staub von Items?")
                .lore(Arrays.asList("§71.» Item welches zerlegt werden soll auswählen", "§72.» Die Vergütung kann im Feld", "      §f\"Deine Rechnung§7\" eingesehen werden", "§73.» Bestätige das Zerlegen mit einem Klick", "§7      auf \"§fZerlegen lassen§7\""))
                .setTexture("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMmUzZjUwYmE2MmNiZGEzZWNmNTQ3OWI2MmZlZGViZDYxZDc2NTg5NzcxY2MxOTI4NmJmMjc0NWNkNzFlNDdjNiJ9fX0=")
                .getItemStack();
        ItemStack insertItemHere = new LostItemBuilderAPI(Material.BARRIER).name("§cBitte wähle ein Item aus!").getItemStack();
        ItemStack resultItem = new LostItemBuilderAPI(Material.BARRIER).name("§cKein Item ausgewählt!").getItemStack();
        this.dismantInventory = Bukkit.createInventory(this.player, 27, "§fItem zerlegen lassen");
        this.dismantInventory.setItem(11, insertItemHere);
        this.dismantInventory.setItem(15, resultItem);
        this.dismantInventory.setItem(22, helpItem);
        for (int i = 0; i < this.dismantInventory.getSize(); i++) {
            if (this.dismantInventory.getItem(i) == null) {
                this.dismantInventory.setItem(i, this.placeHolder);
            }
        }
        this.player.openInventory(this.dismantInventory);
    }

    private void openCraftFinalInventory(ItemStack itemStack) {
        //TODO!!!!!
        ItemStack helpItem = new LostItemBuilderAPI(Material.SKULL_ITEM)
                .name("§c§LWie funktioniert das Herstellen von Items?")
                .lore(Arrays.asList("§71.» Seltenheit auswählen.", "§72.» Eine Itemart auswählen (Helm, Schwert, etc.)", "§73.» Bestätige den Kauf mit einem Klick", "§7      auf \"§fItem herstellen!§7\""))
                .setTexture("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMmUzZjUwYmE2MmNiZGEzZWNmNTQ3OWI2MmZlZGViZDYxZDc2NTg5NzcxY2MxOTI4NmJmMjc0NWNkNzFlNDdjNiJ9fX0=")
                .getItemStack();
        this.craftInventoryFinal = Bukkit.createInventory(this.player, 54, "§fItem herstellen | " + rarityTypee.getColor() + rarityTypee.getName());

        this.craftInventoryFinal.setItem(4, new LostItemBuilderAPI(itemStack).clone().hideAttributes().getItemStack());
        this.craftInventoryFinal.setItem(45, helpItem);

        for (int i = 0; i < this.craftInventoryFinal.getSize(); i++) {
            if (this.craftInventoryFinal.getItem(i) == null) {
                this.craftInventoryFinal.setItem(i, this.placeHolder);
            }
        }
        this.player.openInventory(this.craftInventoryFinal);
    }

    private void openSelectRarityInventory() {
        ItemStack helpItem = new LostItemBuilderAPI(Material.SKULL_ITEM)
                .name("§c§LWie funktioniert das Herstellen von Items?")
                .lore(Arrays.asList("§71.» Seltenheit auswählen.", "§72.» Eine Itemart auswählen (Helm, Schwert, etc.)", "§73.» Bestätige den Kauf mit einem Klick", "§7      auf \"§fItem herstellen!§7\""))
                .setTexture("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMmUzZjUwYmE2MmNiZGEzZWNmNTQ3OWI2MmZlZGViZDYxZDc2NTg5NzcxY2MxOTI4NmJmMjc0NWNkNzFlNDdjNiJ9fX0=")
                .getItemStack();
        this.raritySelectInventory = Bukkit.createInventory(this.player, 54, "§fItem herstellen | Seltenheit auswählen");
        this.raritySelectInventory.setItem(31, helpItem);
        this.raritySelectInventory.setItem(47, new LostItemBuilderAPI(Material.INK_SACK, (byte) 7).name(RarityType.USUALLY.getColor() + RarityType.USUALLY.getName()).lore(Arrays.asList("", "§7» Stelle ein Item mit der Seltenheitsstufe", "   §7" + RarityType.USUALLY.getName() + " her.")).addNBTData("RaritySelector", RarityType.USUALLY.name()).getItemStack());
        this.raritySelectInventory.setItem(27, new LostItemBuilderAPI(Material.INK_SACK, (byte) 8).name(RarityType.MAGIC.getColor() + RarityType.MAGIC.getName()).lore(Arrays.asList("", "§7» Stelle ein Item mit der Seltenheitsstufe", "   §7" + RarityType.MAGIC.getName() + " her.")).addNBTData("RaritySelector", RarityType.MAGIC.name()).getItemStack());
        this.raritySelectInventory.setItem(10, new LostItemBuilderAPI(Material.INK_SACK, (byte) 12).name(RarityType.RARE.getColor() + RarityType.RARE.getName()).lore(Arrays.asList("", "§7» Stelle ein Item mit der Seltenheitsstufe", "   §7" + RarityType.RARE.getName() + " her.")).addNBTData("RaritySelector", RarityType.RARE.name()).getItemStack());
        this.raritySelectInventory.setItem(3, new LostItemBuilderAPI(Material.INK_SACK, (byte) 6).name(RarityType.UNCOMMON.getColor() + RarityType.UNCOMMON.getName()).lore(Arrays.asList("", "§7» Stelle ein Item mit der Seltenheitsstufe", "   §7" + RarityType.UNCOMMON.getName() + " her.")).addNBTData("RaritySelector", RarityType.UNCOMMON.name()).getItemStack());
        this.raritySelectInventory.setItem(5, new LostItemBuilderAPI(Material.INK_SACK, (byte) 13).name(RarityType.EPIC.getColor() + RarityType.EPIC.getName()).lore(Arrays.asList("", "§7» Stelle ein Item mit der Seltenheitsstufe", "   §7" + RarityType.EPIC.getName() + " her.")).addNBTData("RaritySelector", RarityType.EPIC.name()).getItemStack());
        this.raritySelectInventory.setItem(16, new LostItemBuilderAPI(Material.INK_SACK, (byte) 5).name(RarityType.MYSTIC.getColor() + RarityType.MYSTIC.getName()).lore(Arrays.asList("", "§7» Stelle ein Item mit der Seltenheitsstufe", "   §7" + RarityType.MYSTIC.getName() + " her.")).addNBTData("RaritySelector", RarityType.MYSTIC.name()).getItemStack());
        this.raritySelectInventory.setItem(35, new LostItemBuilderAPI(Material.INK_SACK, (byte) 14).name(RarityType.LEGENDARY.getColor() + RarityType.LEGENDARY.getName()).lore(Arrays.asList("", "§7» Stelle ein Item mit der Seltenheitsstufe", "   §7" + RarityType.LEGENDARY.getName() + " her.")).addNBTData("RaritySelector", RarityType.LEGENDARY.name()).getItemStack());
        this.raritySelectInventory.setItem(51, new LostItemBuilderAPI(Material.INK_SACK, (byte) 1).name(RarityType.PHENOMENAL.getColor() + RarityType.PHENOMENAL.getName()).lore(Arrays.asList("", "§7» Stelle ein Item mit der Seltenheitsstufe", "   §7" + RarityType.PHENOMENAL.getName() + " her.")).addNBTData("RaritySelector", RarityType.PHENOMENAL.name()).getItemStack());
        for (int i = 0; i < this.raritySelectInventory.getSize(); i++) {
            if (this.raritySelectInventory.getItem(i) == null) {
                this.raritySelectInventory.setItem(i, this.placeHolder);
            }
        }
        this.player.openInventory(this.raritySelectInventory);
    }

    private void openCraftInventory() {
        ItemStack helpItem = new LostItemBuilderAPI(Material.SKULL_ITEM)
                .name("§c§LWie funktioniert das Herstellen von Items?")
                .lore(Arrays.asList("§71.» Seltenheit auswählen.", "§72.» Eine Itemart auswählen (Helm, Schwert, etc.)", "§73.» Bestätige den Kauf mit einem Klick", "§7      auf \"§fItem herstellen!§7\""))
                .setTexture("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMmUzZjUwYmE2MmNiZGEzZWNmNTQ3OWI2MmZlZGViZDYxZDc2NTg5NzcxY2MxOTI4NmJmMjc0NWNkNzFlNDdjNiJ9fX0=")
                .getItemStack();

        this.craftInventory = Bukkit.createInventory(this.player, 54, "§fItem herstellen");

        this.selHelmet = new LostItemBuilderAPI(Material.DIAMOND_HELMET).name("§fStelle mir einen Helm her").lore(Arrays.asList("", "§7» Seltenheit: " + rarityTypee.getColor() + rarityTypee.getName())).hideAttributes().getItemStack();
        this.selChest = new LostItemBuilderAPI(Material.DIAMOND_CHESTPLATE).name("§fStelle mir eine Brustplatte her").lore(Arrays.asList("", "§7» Seltenheit: " + rarityTypee.getColor() + rarityTypee.getName())).hideAttributes().getItemStack();
        this.selLeggins = new LostItemBuilderAPI(Material.DIAMOND_LEGGINGS).name("§fStelle mir eine Hose her").lore(Arrays.asList("", "§7» Seltenheit: " + rarityTypee.getColor() + rarityTypee.getName())).hideAttributes().getItemStack();
        this.selBoots = new LostItemBuilderAPI(Material.DIAMOND_BOOTS).name("§fStelle mir ein paar Schuhe her").lore(Arrays.asList("", "§7» Seltenheit: " + rarityTypee.getColor() + rarityTypee.getName())).hideAttributes().getItemStack();
        this.selSword = new LostItemBuilderAPI(Material.DIAMOND_SWORD).name("§fStelle mir ein Schwert her").lore(Arrays.asList("", "§7» Seltenheit: " + rarityTypee.getColor() + rarityTypee.getName())).hideAttributes().getItemStack();
        this.selBow = new LostItemBuilderAPI(Material.BOW).name("§fStelle mir einen Bogen her").lore(Arrays.asList("", "§7» Seltenheit: " + rarityTypee.getColor() + rarityTypee.getName())).hideAttributes().getItemStack();
        this.craftInventory.setItem(13, this.selHelmet);
        this.craftInventory.setItem(22, this.selChest);
        this.craftInventory.setItem(31, this.selLeggins);
        this.craftInventory.setItem(40, this.selBoots);
        this.craftInventory.setItem(20, this.selSword);
        this.craftInventory.setItem(24, this.selBow);
        this.craftInventory.setItem(45, helpItem);
        for (int i = 0; i < this.craftInventory.getSize(); i++) {
            if (this.craftInventory.getItem(i) == null) {
                this.craftInventory.setItem(i, this.placeHolder);
            }
        }
        this.player.openInventory(this.craftInventory);
    }

    public void openInventory(Player player) {
        this.player = player;
        this.inventory = Bukkit.createInventory(player, 27, "§fWaffenschmied");
        this.inventory.setItem(10, this.repairItem);
        this.inventory.setItem(12, this.dismantleItem);
        this.inventory.setItem(14, this.craftItem);
        this.inventory.setItem(16, this.revalueItem);
        for (int i = 0; i < this.inventory.getSize(); i++) {
            if (this.inventory.getItem(i) == null) {
                this.inventory.setItem(i, this.placeHolder);
            }
        }
        player.openInventory(inventory);
    }

    private void openRapairInventory() {
        ItemStack helpItem = new LostItemBuilderAPI(Material.SKULL_ITEM)
                .name("§c§LWie funktioniert das Reparieren?")
                .lore(Arrays.asList("§71.» Item welches du reparieren möchtest anklicken.", "§72.» Die Reparaturkosten können im Feld", "      §f\"Deine Rechnung§7\" eingesehen werden", "§73.» Bestätige die Reparatur mit einem Klick", "§7      auf \"§fReparieren lassen§7\""))
                .setTexture("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMmUzZjUwYmE2MmNiZGEzZWNmNTQ3OWI2MmZlZGViZDYxZDc2NTg5NzcxY2MxOTI4NmJmMjc0NWNkNzFlNDdjNiJ9fX0=")
                .getItemStack();
        ItemStack insertItemHere = new LostItemBuilderAPI(Material.BARRIER).name("§cBitte wähle ein Item aus!").getItemStack();
        ItemStack resultItem = new LostItemBuilderAPI(Material.BARRIER).name("§cKein Item ausgewählt!").getItemStack();
        this.repairInventory = Bukkit.createInventory(this.player, 27, "§fItem reparieren");
        this.repairInventory.setItem(11, insertItemHere);
        this.repairInventory.setItem(15, resultItem);
        this.repairInventory.setItem(22, helpItem);
        for (int i = 0; i < this.repairInventory.getSize(); i++) {
            if (this.repairInventory.getItem(i) == null) {
                this.repairInventory.setItem(i, this.placeHolder);
            }
        }
        this.player.openInventory(this.repairInventory);
    }
}
