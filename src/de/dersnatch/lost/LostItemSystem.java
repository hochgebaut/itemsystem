package de.dersnatch.lost;

import de.dersnatch.lost.command.LostItemCommand;
import de.dersnatch.lost.listener.BlockEvent;
import de.dersnatch.lost.listener.ExpChangeEvent;
import de.dersnatch.lost.listener.HitEvent;
import de.dersnatch.lost.listener.SoulboundEvent;
import de.dersnatch.lost.villager.VillagerInventory;
import de.dersnatch.lost.villager.VillagerListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class LostItemSystem extends JavaPlugin {

    public static LostItemSystem instance;

    @Override
    public void onEnable() {
        instance = this;
        Bukkit.getPluginManager().registerEvents(new VillagerListener(), this);
        Bukkit.getPluginManager().registerEvents(new VillagerInventory(), this);
        Bukkit.getPluginManager().registerEvents(new BlockEvent(), this);
        Bukkit.getPluginManager().registerEvents(new ExpChangeEvent(), this);
        Bukkit.getPluginManager().registerEvents(new HitEvent(), this);
        Bukkit.getPluginManager().registerEvents(new SoulboundEvent(), this);
        getCommand("lostitemsystem").setExecutor(new LostItemCommand());
    }

    @Override
    public void onDisable() {

    }

    public static LostItemSystem getInstance() {
        return instance;
    }
}
