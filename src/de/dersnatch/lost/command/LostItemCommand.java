package de.dersnatch.lost.command;

import de.dersnatch.lost.LostItemSystem;
import de.dersnatch.lost.enums.Item;
import de.dersnatch.lost.enums.LostItemType;
import de.dersnatch.lost.enums.RarityType;
import de.dersnatch.lost.manager.LostItem;
import de.dersnatch.lost.util.ItemSkulls;
import de.dersnatch.lost.util.PageInventory;
import de.dersnatch.lost.villager.VillagerListener;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

/**
 * Copyright (c) 2017 DerSnatch
 * created on 30.07.2017-10:49
 **/
public class LostItemCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (commandSender instanceof Player) {
            Player player = (Player) commandSender;
            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("list")) {
                    ItemStack nextPage;
                    nextPage = ItemSkulls.getRawSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZjJmM2EyZGZjZTBjM2RhYjdlZTEwZGIzODVlNTIyOWYxYTM5NTM0YThiYTI2NDYxNzhlMzdjNGZhOTNiIn19fQ==");
                    ItemMeta nextPageMeta = nextPage.getItemMeta();
                    nextPageMeta.setDisplayName("§8Nächste");
                    nextPage.setItemMeta(nextPageMeta);
                    ItemStack lastPage;
                    lastPage = ItemSkulls.getRawSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYmIwZjZlOGFmNDZhYzZmYWY4ODkxNDE5MWFiNjZmMjYxZDY3MjZhNzk5OWM2MzdjZjJlNDE1OWZlMWZjNDc3In19fQ==");
                    ItemMeta lastPageMeta = lastPage.getItemMeta();
                    lastPageMeta.setDisplayName("§8Zurück");
                    lastPage.setItemMeta(lastPageMeta);
                    PageInventory inventory = new PageInventory("§8Lost | ItemSystem", 54, nextPage, lastPage, LostItemSystem.getInstance());
                    Arrays.stream(Item.values()).forEach(item -> inventory.addItem(new LostItem(item).build(true)));
                    inventory.open(player);
                } else if (args[0].equalsIgnoreCase("randomitem1")) {
                    player.getInventory().addItem(new LostItem(RarityType.USUALLY).build(true));
                } else if (args[0].equalsIgnoreCase("randomitem2")) {
                    player.getInventory().addItem(new LostItem(LostItemType.ARMOR).build(true));
                } else if (args[0].equalsIgnoreCase("randomitem3")) {
                    player.getInventory().addItem(new LostItem().build(true));
                } else if (args[0].equalsIgnoreCase("spawnvillager")) {
                    VillagerListener.spawnMob(player.getLocation());
                    player.sendMessage("§aDu hast den Villager zum Reparieren gespawnt!");
                } else {
                    player.sendMessage("§c» Falsche Benutzung!");
                    player.sendMessage("§7» /lostitemsystem list");
                    player.sendMessage("§7» /lostitemsystem spawnvillager");
                    player.sendMessage("§7» /lostitemsystem despawnvillager");
                }
            } else {
                player.sendMessage("§c» Falsche Benutzung!");
                player.sendMessage("§7» /lostitemsystem list");
                player.sendMessage("§7» /lostitemsystem spawnvillager");
                player.sendMessage("§7» /lostitemsystem despawnvillager");
            }
        }
        return true;
    }
}
