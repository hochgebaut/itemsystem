package de.dersnatch.lost.manager;

import de.dersnatch.lost.enums.EffektType;
import de.dersnatch.lost.enums.Item;
import de.dersnatch.lost.enums.LostItemType;
import net.minecraft.server.v1_12_R1.NBTTagCompound;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class ItemManager {

    public static boolean isCustomItem(ItemStack itemStack) {
        net.minecraft.server.v1_12_R1.ItemStack copy = CraftItemStack.asNMSCopy(itemStack);
        if (!copy.hasTag()) {
            return false;
        }
        NBTTagCompound compound = copy.getTag();
        if (compound.hasKey("CustomItem")) {
            return true;
        }
        return false;
    }

    public static int getMaxDurability(ItemStack itemStack) {
        if (itemStack != null) {
            ItemMeta itemMeta = itemStack.getItemMeta();
            if (itemMeta != null) {
                List<String> lore = itemMeta.getLore();
                if (lore != null) {
                    for (String line : lore) {
                        if (line.startsWith("§7Haltbarkeit: §f")) {
                            String[] durabilityLine = line.split("§f");
                            String[] durab = durabilityLine[1].split("/");
                            int durability = Integer.parseInt(durab[1]);
                            return durability;
                        }
                    }
                }
            }
        }
        return 0;
    }

    public static int getCurrentDurability(ItemStack itemStack) {
        if (itemStack != null) {
            ItemMeta itemMeta = itemStack.getItemMeta();
            if (itemMeta != null) {
                List<String> lore = itemMeta.getLore();
                if (lore != null) {
                    for (String line : lore) {
                        if (line.startsWith("§7Haltbarkeit: §f")) {
                            String[] durabilityLine = line.split("§f");
                            String[] durab = durabilityLine[1].split("/");
                            int durability = Integer.parseInt(durab[0]);
                            return durability;
                        }
                    }
                }
            }
        }
        return 0;
    }

    public static int getMaxAttackDamage(ItemStack itemStack) {
        if (!isCustomItem(itemStack))
            return 0;
        return Integer.parseInt(getNBTData(itemStack, "MaxAttackDamage"));
    }

    public static int getCurrentAttackDamge(ItemStack itemStack) {
        if (!isCustomItem(itemStack))
            return 0;
        return Integer.parseInt(getNBTData(itemStack, "CurrentAttackDamage"));
    }

    public static int getMaxAttackSpeed(ItemStack itemStack) {
        if (!isCustomItem(itemStack))
            return 0;
        return Integer.parseInt(getNBTData(itemStack, "MaxAttackSpeed"));
    }

    public static double getCurrentAttackSpeed(ItemStack itemStack) {
        if (!isCustomItem(itemStack))
            return 0;
        return Double.parseDouble(getNBTData(itemStack, "CurrentAttackSpeed"));
    }

    public static int getMaxProtection(ItemStack itemStack) {
        if (!isCustomItem(itemStack))
            return 0;
        return Integer.parseInt(getNBTData(itemStack, "MaxProtection"));
    }

    public static int getCurrentProtection(ItemStack itemStack) {
        if (!isCustomItem(itemStack))
            return 0;
        return Integer.parseInt(getNBTData(itemStack, "CurrentProtection"));
    }

    public static int getCurrentReValueLvl(ItemStack itemStack) {
        if (!isCustomItem(itemStack))
            return 0;
        return Integer.parseInt(getNBTData(itemStack, "CurrentReValueLvl"));
    }

    public static ItemStack setAttackDamage(ItemStack itemStack, int i) {
        int newAttackDamage = getCurrentAttackDamge(itemStack) + i;
        itemStack = removeNBTData(itemStack, "CurrentAttackDamage");
        itemStack = addNBTData(itemStack, "CurrentAttackDamage", String.valueOf(newAttackDamage));
        return itemStack;
    }

    public static ItemStack setAttackSpeed(ItemStack itemStack, double i) {
        double newAttackSpeed = getCurrentAttackSpeed(itemStack) + i;
        itemStack = removeNBTData(itemStack, "CurrentAttackSpeed");
        itemStack = addNBTData(itemStack, "CurrentAttackSpeed", String.valueOf(newAttackSpeed));
        return itemStack;
    }

    public static ItemStack setRevalueLvl(ItemStack itemStack, int i) {
        int newRevalueLvl = getCurrentReValueLvl(itemStack) + i;
        itemStack = removeNBTData(itemStack, "CurrentReValueLvl");
        itemStack = addNBTData(itemStack, "CurrentReValueLvl", String.valueOf(newRevalueLvl));
        return itemStack;
    }

    public static String getNBTData(ItemStack itemStack, String key) {
        net.minecraft.server.v1_12_R1.ItemStack copy = CraftItemStack.asNMSCopy(itemStack);
        if (!copy.hasTag()) {
            return null;
        }
        NBTTagCompound compound = copy.getTag();
        if (!compound.hasKey(key)) {
            return null;
        }
        return compound.getString(key);
    }

    public static ItemStack addNBTData(ItemStack itemStack, String key, String value) {
        net.minecraft.server.v1_12_R1.ItemStack nmsCopy = CraftItemStack.asNMSCopy(itemStack);
        NBTTagCompound nbtTagCompound = (nmsCopy.hasTag()) ? nmsCopy.getTag() : new NBTTagCompound();
        nbtTagCompound.setBoolean("Unbreakable", true);
        nbtTagCompound.setString(key, value);
        nbtTagCompound.setInt("HideFlags", 126);
        nmsCopy.setTag(nbtTagCompound);
        return CraftItemStack.asBukkitCopy(nmsCopy);
    }

    public static ItemStack addNBTData(ItemStack itemStack, String key, boolean value) {
        net.minecraft.server.v1_12_R1.ItemStack nmsCopy = CraftItemStack.asNMSCopy(itemStack);
        NBTTagCompound nbtTagCompound = (nmsCopy.hasTag()) ? nmsCopy.getTag() : new NBTTagCompound();
        nbtTagCompound.setBoolean("Unbreakable", true);
        nbtTagCompound.setBoolean(key, value);
        nbtTagCompound.setInt("HideFlags", 126);
        nmsCopy.setTag(nbtTagCompound);
        return CraftItemStack.asBukkitCopy(nmsCopy);
    }

    private static ItemStack removeNBTData(ItemStack itemStack, String key) {
        net.minecraft.server.v1_12_R1.ItemStack nmsCopy = CraftItemStack.asNMSCopy(itemStack);
        NBTTagCompound nbtTagCompound = (nmsCopy.hasTag()) ? nmsCopy.getTag() : new NBTTagCompound();
        if (nbtTagCompound.hasKey(key)) {
            nbtTagCompound.remove(key);
        }
        nmsCopy.setTag(nbtTagCompound);
        return CraftItemStack.asBukkitCopy(nmsCopy);
    }

    public static void setDurability(ItemStack itemStack, int o) {
        if (itemStack != null) {
            ItemMeta itemMeta = itemStack.getItemMeta();
            if (itemMeta.getLore() != null) {
                List<String> lore = itemMeta.getLore();
                if (lore != null) {
                    for (int i = 0; i < lore.size(); i++) {
                        if (lore.get(i).startsWith("§7Haltbarkeit: §f")) {
                            int newDurability = getCurrentDurability(itemStack) + o;
                            lore.set(i, "§7Haltbarkeit: §f" + newDurability + "/" + getMaxDurability(itemStack));
                            itemMeta.setLore(lore);
                            itemStack.setItemMeta(itemMeta);
                            return;
                        }
                    }
                }
            }
        }
    }

    public static boolean isWeapon(ItemStack usedItem) {
        Item item = Item.valueOf(getNBTData(usedItem, "UUID"));
        if (item.getItemType() == LostItemType.WEAPON)
            return true;
        return false;
    }

    public static boolean isBow(ItemStack usedItem) {
        Item item = Item.valueOf(getNBTData(usedItem, "UUID"));
        if (item.getItemType() == LostItemType.BOW)
            return true;
        return false;
    }

    public static boolean isTool(ItemStack usedItem) {
        Item item = Item.valueOf(getNBTData(usedItem, "UUID"));
        if (item.getItemType() == LostItemType.TOOL)
            return true;
        return false;
    }

    public static boolean isArmor(ItemStack usedItem) {
        Item item = Item.valueOf(getNBTData(usedItem, "UUID"));
        if (item.getItemType() == LostItemType.ARMOR)
            return true;
        return false;
    }

    public static boolean hasEnchantment(ItemStack item, EffektType type) {
        net.minecraft.server.v1_12_R1.ItemStack copy = CraftItemStack.asNMSCopy(item);
        if (!copy.hasTag()) {
            return false;
        }
        NBTTagCompound compound = copy.getTag();
        if (!compound.hasKey("LostEffekt_" + type.name())) {
            return false;
        }
        return true;
    }

    public static int getEffektAmount(ItemStack itemStack, EffektType effektType) {
        return Integer.parseInt(getNBTData(itemStack, effektType.name() + "_amount"));
    }
}
