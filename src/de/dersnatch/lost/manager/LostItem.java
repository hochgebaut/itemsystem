package de.dersnatch.lost.manager;

import de.dersnatch.lost.enums.Item;
import de.dersnatch.lost.enums.LostItemType;
import de.dersnatch.lost.enums.RarityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LostItem {

    private Item item;
    private ItemStack itemStack;
    private ItemMeta itemMeta;
    private boolean random;
    private int attackDamage;
    private int protection;
    private int durability;

    public LostItem() {
        this.item = getRandomItem();
        this.itemStack = new ItemStack(item.getMaterial());
        this.itemMeta = this.itemStack.getItemMeta();
        this.itemMeta.setDisplayName(item.getRarityType().getColor() + item.getName());
    }

    public LostItem(Item item) {
        this.item = item;
        this.itemStack = new ItemStack(item.getMaterial());
        this.itemMeta = this.itemStack.getItemMeta();
        this.itemMeta.setDisplayName(item.getRarityType().getColor() + item.getName());
    }

    public LostItem(RarityType rarityType) {
        this.item = getRandomItem(rarityType);
        this.itemStack = new ItemStack(item.getMaterial());
        this.itemMeta = this.itemStack.getItemMeta();
        this.itemMeta.setDisplayName(item.getRarityType().getColor() + item.getName());
    }

    public LostItem(LostItemType itemType) {
        this.item = getRandomItem(itemType);
        this.itemStack = new ItemStack(item.getMaterial());
        this.itemMeta = this.itemStack.getItemMeta();
        this.itemMeta.setDisplayName(item.getRarityType().getColor() + item.getName());
    }

    public LostItem(LostItemType itemType, RarityType rarityType) {
        this.item = getRandomItem(itemType, rarityType);
        this.itemStack = new ItemStack(item.getMaterial());
        this.itemMeta = this.itemStack.getItemMeta();
        this.itemMeta.setDisplayName(item.getRarityType().getColor() + item.getName());
    }

    private Item getRandomItem(LostItemType itemType, RarityType rarityType) {
        return null;
    }

    private Item getRandomItem() {
        int i;
        int min = 0;
        int max = getRandomItems().size() - 1;
        i = (int) (Math.random() * ((max - min) + 1) + min);
        return getRandomItems().get(i);
    }

    private Item getRandomItem(LostItemType itemType) {
        int i;
        int min = 0;
        int max = getRandomItems(itemType).size() - 1;
        i = (int) (Math.random() * ((max - min) + 1) + min);
        return getRandomItems(itemType).get(i);
    }

    private List<Item> getRandomItems() {
        List<Item> items = new ArrayList<>();
        Arrays.stream(Item.values()).forEach(item -> items.add(item));
        return items;
    }

    private List<Item> getRandomItems(LostItemType itemType) {
        List<Item> items = new ArrayList<>();
        Arrays.stream(Item.values()).forEach(item -> {
            if (item.getItemType().equals(itemType)) {
                items.add(item);
            }
        });
        return items;
    }

    private Item getRandomItem(RarityType rarityType) {
        int i;
        int min = 0;
        int max = getRandomItems(rarityType).size() - 1;
        i = (int) (Math.random() * ((max - min) + 1) + min);
        return getRandomItems(rarityType).get(i);
    }

    private List<Item> getRandomItems(RarityType rarityType) {
        List<Item> items = new ArrayList<>();
        Arrays.stream(Item.values()).forEach(item -> {
            if (item.getRarityType().equals(rarityType)) {
                items.add(item);
            }
        });
        return items;
    }

    public ItemStack build(boolean randomValues) {
        this.random = randomValues;
        if (this.random == true) {
            if (item.getItemType().equals(LostItemType.WEAPON) || item.getItemType().equals(LostItemType.BOW) || item.getItemType().equals(LostItemType.TOOL)) {
                this.attackDamage = randomAttackDamage(item);
                this.durability = randomDurability(item);
            } else if (item.getItemType().equals(LostItemType.ARMOR)) {
                this.protection = randomProtection(item);
                this.durability = randomDurability(item);
            }
        } else {
            if (item.getItemType().equals(LostItemType.WEAPON) || item.getItemType().equals(LostItemType.BOW) || item.getItemType().equals(LostItemType.TOOL)) {
                this.attackDamage = item.getMaxAttackDamage();
                this.durability = item.getMaxDurability();
            } else if (item.getItemType().equals(LostItemType.ARMOR)) {
                this.protection = item.getMaxProtection();
                this.durability = item.getMaxDurability();
            }
        }
        this.itemMeta.setLore(getLore(this.item));
        this.itemStack.setItemMeta(this.itemMeta);

        this.itemStack = ItemManager.addNBTData(this.itemStack, "CustomItem", true);
        this.itemStack = ItemManager.addNBTData(this.itemStack, "UUID", this.item.name());

        if (this.random == true) {
            if (item.getItemType().equals(LostItemType.WEAPON) || item.getItemType().equals(LostItemType.BOW) || item.getItemType().equals(LostItemType.TOOL)) {
                this.itemStack = ItemManager.addNBTData(itemStack, "CurrentAttackDamage", String.valueOf(this.attackDamage));
                this.itemStack = ItemManager.addNBTData(itemStack, "MaxAttackDamage", String.valueOf(this.item.getMaxDurability()));
                this.itemStack = ItemManager.addNBTData(itemStack, "CurrentReValueLvl", String.valueOf(0));
                this.itemStack = ItemManager.addNBTData(itemStack, "MaxReValueLvl", String.valueOf(3));
                this.itemStack = ItemManager.addNBTData(itemStack, "CurrentAttackSpeed", String.valueOf(this.item.getAttackSpeed()));
            } else if (item.getItemType().equals(LostItemType.ARMOR)) {
                this.itemStack = ItemManager.addNBTData(itemStack, "CurrentProtection", String.valueOf(this.protection));
                this.itemStack = ItemManager.addNBTData(itemStack, "MaxProtection", String.valueOf(this.item.getMaxProtection()));
                this.itemStack = ItemManager.addNBTData(itemStack, "CurrentReValueLvl", String.valueOf(0));
                this.itemStack = ItemManager.addNBTData(itemStack, "MaxReValueLvl", String.valueOf(3));
            }
        } else {
            if (item.getItemType().equals(LostItemType.WEAPON) || item.getItemType().equals(LostItemType.BOW) || item.getItemType().equals(LostItemType.TOOL)) {
                this.itemStack = ItemManager.addNBTData(itemStack, "CurrentAttackDamage", String.valueOf(this.item.getMaxDurability()));
                this.itemStack = ItemManager.addNBTData(itemStack, "MaxAttackDamage", String.valueOf(this.item.getMaxDurability()));
                this.itemStack = ItemManager.addNBTData(itemStack, "CurrentReValueLvl", String.valueOf(0));
                this.itemStack = ItemManager.addNBTData(itemStack, "MaxReValueLvl", String.valueOf(3));
                this.itemStack = ItemManager.addNBTData(itemStack, "CurrentAttackSpeed", String.valueOf(this.item.getAttackSpeed()));
            } else if (item.getItemType().equals(LostItemType.ARMOR)) {
                this.itemStack = ItemManager.addNBTData(itemStack, "CurrentProtection", String.valueOf(this.item.getMaxDurability()));
                this.itemStack = ItemManager.addNBTData(itemStack, "MaxProtection", String.valueOf(this.item.getMaxProtection()));
                this.itemStack = ItemManager.addNBTData(itemStack, "CurrentReValueLvl", String.valueOf(0));
                this.itemStack = ItemManager.addNBTData(itemStack, "MaxReValueLvl", String.valueOf(3));
            }
        }

        Arrays.stream(item.getLostEffekts()).forEach(lostEffekt -> {
            this.itemStack = ItemManager.addNBTData(itemStack, "LostEffekt_" + lostEffekt.getEffektType().name(), true);
            if (lostEffekt.getAmount() != 0)
                this.itemStack = ItemManager.addNBTData(itemStack, lostEffekt.getEffektType().name() + "_amount", String.valueOf(lostEffekt.getAmount()));
            if (lostEffekt.getLenght() != 0)
                this.itemStack = ItemManager.addNBTData(itemStack, lostEffekt.getEffektType().name() + "_lenght", String.valueOf(lostEffekt.getLenght()));
            if (lostEffekt.getStrenght() != 0)
                this.itemStack = ItemManager.addNBTData(itemStack, lostEffekt.getEffektType().name() + "_strenght", String.valueOf(lostEffekt.getStrenght()));
        });

        return this.itemStack;
    }

    private static int randomProtection(Item item) {
        int i;
        if (Math.random() < 0.05) {
            i = (int) (Math.random() * ((item.getMaxProtection() - item.getMinProtection()) + 1) + item.getMinProtection());
        } else {
            i = item.getMinProtection();
        }
        return i;
    }

    private static int randomDurability(Item item) {
        int i;
        if (Math.random() < 0.3333) {
            i = (int) (Math.random() * ((item.getMaxDurability() - item.getMinDurability()) + 1) + item.getMinDurability());
        } else {
            i = (int) (Math.random() * (((item.getMaxDurability() / 4 * 3) - item.getMinDurability()) + 1) + item.getMinDurability());
        }
        return i;
    }

    private static int randomAttackDamage(Item item) {
        int i;
        if (Math.random() < 0.05) {
            i = (int) (Math.random() * ((item.getMaxAttackDamage() - item.getMinAttackDamage()) + 1) + item.getMinAttackDamage());
        } else {
            i = item.getMinAttackDamage();
        }
        return i;
    }

    public List<String> getLore(Item item) {
        List<String> lore = new ArrayList<>();
        if (item.getItemType().equals(LostItemType.WEAPON) || item.getItemType().equals(LostItemType.TOOL))
            lore = getWeaponLore(item);
        if (item.getItemType().equals(LostItemType.BOW))
            lore = getBowLore(item);
        if (item.getItemType().equals(LostItemType.ARMOR))
            lore = getArmorLore(item);
        return lore;
    }

    public List<String> getArmorLore(Item item) {
        List<String> lore = new ArrayList<>();
        lore.add("");
        lore.add("§7Verteidigung: §f" + this.protection);
        lore.add("");
        lore.add("§7Seltenheit: §f" + item.getRarityType().getColor() + item.getRarityType().getName());
        lore.add("§7Gewicht: §f" + item.getWeight());
        lore.add("");
        lore.add("§7Haltbarkeit: §f" + this.durability + "/" + item.getMaxDurability());
        lore.add("§7Aufwertung: §f0/3");
        lore.add("");
        lore.add("§7Itemtyp: §f" + item.getItemType().getName());
        lore.add("§7Rüstungsset: §f" + item.getItemSet().getName() + "§f | [§f0§f/4]");
        lore.add("");
        lore.add("§7Eigenschaften:");
        Arrays.stream(item.getLostEffekts()).forEach(lostSetEffect -> {
            lore.add("§7» §f" + lostSetEffect.getEffektType().getName() + " §f- " + ((lostSetEffect.getAmount() == 0 ) ? "§cStärke: §7" + lostSetEffect.getStrenght() + " §cLänge: §7" + lostSetEffect.getLenght()/20 : "sec §cStufe: §7" + lostSetEffect.getAmount()));
        });
        if (item.getLostEffekts() == null || item.getLostEffekts().length < 1) {
            lore.add("§7» §cKeine Eigenschaften");
        }
        lore.add("");
        lore.add("§7Set-Eigenschaften:");
        Arrays.stream(item.getItemSet().getLostEffekts()).forEach(lostSetEffect -> {
            lore.add("§7» §f" + lostSetEffect.getEffektType().getName());
        });
        if (item.getItemSet().getLostEffekts() == null || item.getItemSet().getLostEffekts().length < 1) {
            lore.add("§7» §cKeine Set-Eigenschaften");
        }
        return lore;
    }

    public List<String> getBowLore(Item item) {
        List<String> lore = new ArrayList<>();
        lore.add("");
        lore.add("§7Angriffsschaden: §f" + this.attackDamage);
        lore.add("");
        lore.add("§7Seltenheit: §f" + item.getRarityType().getColor() + item.getRarityType().getName());
        lore.add("§7Gewicht: §f" + item.getWeight());
        lore.add("");
        lore.add("§7Haltbarkeit: §f" + this.durability + "/" + item.getMaxDurability());
        lore.add("§7Aufwertung: §f0/3");
        lore.add("");
        lore.add("§7Itemtyp: §f" + item.getItemType().getName());
        lore.add("");
        lore.add("§7Eigenschaften:");
        Arrays.stream(item.getLostEffekts()).forEach(lostSetEffect -> {
            lore.add("§7» §f" + lostSetEffect.getEffektType().getName() + " §f- " + ((lostSetEffect.getAmount() == 0 ) ? "§cStärke: §7" + lostSetEffect.getStrenght() + " §cLänge: §7" + lostSetEffect.getLenght()/20 : "sec §cStufe: §7" + lostSetEffect.getAmount()));
        });
        if (item.getLostEffekts() == null || item.getLostEffekts().length < 1) {
            lore.add("§7» §cKeine Eigenschaften");
        }
        return lore;
    }

    public List<String> getWeaponLore(Item item) {
        List<String> lore = new ArrayList<>();
        lore.add("");
        lore.add("§7Angriffsschaden: §f" + this.attackDamage);
        lore.add("§7Angriffsgeschwindigkeit: §f" + item.getAttackSpeed());
        lore.add("");
        lore.add("§7Seltenheit: §f" + item.getRarityType().getColor() + item.getRarityType().getName());
        lore.add("§7Gewicht: §f" + item.getWeight());
        lore.add("");
        lore.add("§7Haltbarkeit: §f" + this.durability + "/" + item.getMaxDurability());
        lore.add("§7Aufwertung: §f0/3");
        lore.add("");
        lore.add("§7Itemtyp: §f" + item.getItemType().getName());
        lore.add("");
        lore.add("§7Eigenschaften:");
        Arrays.stream(item.getLostEffekts()).forEach(lostSetEffect -> {
            lore.add("§7» §f" + lostSetEffect.getEffektType().getName() + " §f- " + ((lostSetEffect.getAmount() == 0 ) ? "§cStärke: §7" + lostSetEffect.getStrenght() + " §cLänge: §7" + lostSetEffect.getLenght()/20 + "sec" : "§cStufe: §7" + lostSetEffect.getAmount()));
        });
        if (item.getLostEffekts() == null || item.getLostEffekts().length < 1) {
            lore.add("§7» §cKeine Eigenschaften");
        }
        return lore;
    }

}
