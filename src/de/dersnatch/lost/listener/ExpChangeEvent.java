package de.dersnatch.lost.listener;

import de.dersnatch.lost.enums.EffektType;
import de.dersnatch.lost.manager.ItemManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.inventory.ItemStack;

public class ExpChangeEvent implements Listener {

    @EventHandler
    public void on(PlayerExpChangeEvent event) {
        Player player = event.getPlayer();
        ItemStack item = player.getInventory().getItemInMainHand();
        if (ItemManager.isCustomItem(item) && ItemManager.hasEnchantment(item, EffektType.REPARATUR)) {
            if (ItemManager.getCurrentDurability(item) + event.getAmount() > ItemManager.getMaxDurability(item)) {
                int currentDurability = ItemManager.getCurrentDurability(item);
                int maxDurability = ItemManager.getMaxDurability(item);
                int durabilityToRegenerate = maxDurability - currentDurability;
                ItemManager.setDurability(item, durabilityToRegenerate);
                return;
            }
            ItemManager.setDurability(item, event.getAmount());
        }
    }

}
