package de.dersnatch.lost.listener;

import de.dersnatch.lost.enums.EffektType;
import de.dersnatch.lost.enums.Item;
import de.dersnatch.lost.manager.ItemManager;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HitEvent implements Listener {

    private int protection;

    @EventHandler
    public void on(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
            Player player = (Player) event.getDamager();
            ItemStack usedItem = player.getInventory().getItemInMainHand();
            if (ItemManager.isCustomItem(usedItem)) {
                if (ItemManager.getCurrentDurability(usedItem) == 0) {
                    event.setCancelled(true);
                    player.playSound(player.getLocation(), Sound.ENTITY_ITEM_BREAK, SoundCategory.MASTER, 1F, 1F);
                    return;
                }
                ItemManager.setDurability(usedItem, -1);
                if (event.getEntity() instanceof Player) {
                    Player target = (Player) event.getEntity();
                    removeArmorDurability(target);
                    getProtection(target);
                    Item item = Item.valueOf(ItemManager.getNBTData(usedItem, "UUID"));
                    if (item.getLostEffekts() != null) {
                        Arrays.stream(item.getLostEffekts()).forEach(lostEffekt -> {
                            if (lostEffekt.getEffektType().equals(EffektType.WITHER)) {
                                target.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 40, 2));
                            }
                        });
                    }
                    event.setDamage(ItemManager.getCurrentAttackDamge(usedItem) - protection);
                    target.sendMessage("§cDu hast §7" + (ItemManager.getCurrentAttackDamge(usedItem) - protection) / 2 + " §cHerzen verloren");
                    player.sendMessage("§aDu hast §7" + (ItemManager.getCurrentAttackDamge(usedItem) - protection) / 2 + " §aHerzen abgezogen");
                    if (ItemManager.hasEnchantment(usedItem, EffektType.LEBENSRAUB)) {
                        int lvl = ItemManager.getEffektAmount(usedItem, EffektType.LEBENSRAUB);
                        if (lvl == 1) {
                            if (Math.random() < 0.05) {
                                player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20*5, 2));
                            }
                        } else if (lvl == 2) {
                            if (Math.random() < 0.10) {
                                player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20*5, 2));
                            }
                        }
                    }
                } else {
                    event.setDamage(ItemManager.getCurrentAttackDamge(usedItem));
                    player.sendMessage("§aDu hast §7" + ItemManager.getCurrentAttackDamge(usedItem) / 2 + " §aHerzen abgezogen");
                }
            }
        }
    }

    private void removeArmorDurability(Player target) {
        List<ItemStack> itemStacks = new ArrayList<>();
        itemStacks.add(target.getInventory().getHelmet());
        itemStacks.add(target.getInventory().getChestplate());
        itemStacks.add(target.getInventory().getLeggings());
        itemStacks.add(target.getInventory().getBoots());
        itemStacks.forEach(itemStack -> {
            if (ItemManager.isCustomItem(itemStack)) {
                ItemManager.setDurability(itemStack, -1);
            }
        });
    }

    private void getProtection(Player target) {
        protection = 0;
        List<ItemStack> itemStacks = new ArrayList<>();
        itemStacks.add(target.getInventory().getHelmet());
        itemStacks.add(target.getInventory().getChestplate());
        itemStacks.add(target.getInventory().getLeggings());
        itemStacks.add(target.getInventory().getBoots());
        itemStacks.forEach(itemStack -> {
            if (ItemManager.isCustomItem(itemStack)) {
                protection = protection + ItemManager.getCurrentProtection(itemStack);
            }
        });
    }

}
