package de.dersnatch.lost.listener;

import de.dersnatch.lost.enums.EffektType;
import de.dersnatch.lost.manager.ItemManager;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public class BlockEvent implements Listener {

    @EventHandler
    public void on(BlockBreakEvent event) {
        Player player = event.getPlayer();
        ItemStack usedItem = player.getInventory().getItemInMainHand();
        if (ItemManager.isCustomItem(usedItem)) {
            if (ItemManager.isTool(usedItem)) {
                if (ItemManager.getCurrentDurability(usedItem) == 0) {
                    event.setCancelled(true);
                    player.playSound(player.getLocation(), Sound.ENTITY_ITEM_BREAK, SoundCategory.MASTER, 1F, 1F);
                    return;
                }
                ItemManager.setDurability(usedItem, -1);
                if (ItemManager.hasEnchantment(usedItem, EffektType.GLÜCKSPILZ)) {
                    if (event.getBlock().getType() != null && event.getBlock().getType().equals(Material.STONE)) {
                        int lvl = ItemManager.getEffektAmount(usedItem, EffektType.GLÜCKSPILZ);
                        if (lvl == 1) {
                            if (Math.random() < 0.02) {
                                player.sendMessage("§aDu hast ein unidentifiziertes Item bekommen!");
                                player.sendMessage("§c§LTODO!");
                            }
                        } else if (lvl == 2) {
                            if (Math.random() < 0.04) {
                                player.sendMessage("§aDu hast ein unidentifiziertes Item bekommen!");
                                player.sendMessage("§c§LTODO!");
                            }
                        } else if (lvl == 3) {
                            if (Math.random() < 0.08) {
                                player.sendMessage("§aDu hast ein unidentifiziertes Item bekommen!");
                                player.sendMessage("§c§LTODO!");
                            }
                        } else {
                            if (Math.random() < 0.005) {
                                player.sendMessage("§aDu hast ein unidentifiziertes Item bekommen!");
                                player.sendMessage("§c§LTODO!");
                            }
                        }
                    }
                }
            } else {
                event.setCancelled(true);
                return;
            }
        }
    }

}
