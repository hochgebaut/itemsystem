package de.dersnatch.lost.listener;

import de.dersnatch.lost.LostItemSystem;
import de.dersnatch.lost.enums.EffektType;
import de.dersnatch.lost.manager.ItemManager;
import de.dersnatch.lost.manager.LostItem;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class SoulboundEvent implements Listener {

    private HashMap<Player, List<ItemStack>> keepItems = new HashMap<>();

    @EventHandler
    public void on(PlayerDeathEvent event) {
        if (!event.getEntity().getPlayer().getWorld().isGameRule("keepInventory")) {
            List<ItemStack> items = new ArrayList<>();
            Arrays.stream(event.getEntity().getInventory().getContents()).forEach(itemStack -> {
                if (ItemManager.hasEnchantment(itemStack, EffektType.SOULBOUND)) {
                    items.add(itemStack);
                }
                keepItems.put(event.getEntity().getPlayer(), items);
            });
        }
    }

    @EventHandler
    public void on(PlayerRespawnEvent event) {
        if (keepItems.containsKey(event.getPlayer())) {
            keepItems.get(event.getPlayer()).stream().forEach(itemStack -> {
                event.getPlayer().getInventory().addItem(itemStack);
            });
        }
    }
}
