package de.dersnatch.lost.util;

import de.dersnatch.lost.enums.Item;
import de.dersnatch.lost.manager.ItemManager;
import de.dersnatch.lost.manager.LostItem;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.*;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Copyright (c) 2017 DerSnatch
 * created on 30.07.2017-10:44
 **/
public class PageInventory {

    private LinkedList<Page> pages;
    private String title;
    private int size;
    private ItemStack next_page_item = new ItemStack(Material.ANVIL);
    private ItemStack last_page_item = new ItemStack(Material.ANVIL);
    private Map<String, PageInventory> playersThatOpenedInventories;
    private Listener listener;
    private Plugin plugin;

    public PageInventory(String title, int size, ItemStack next_page_item, ItemStack last_page_item, Plugin plugin) {
        this.next_page_item = next_page_item;
        this.last_page_item = last_page_item;
        this.title = title;
        this.size = size;
        this.plugin = plugin;
        pages = new LinkedList<>();
        listener = new Listener() {
            @EventHandler
            public void on(InventoryClickEvent e) {
                if (playersThatOpenedInventories.containsKey(e.getWhoClicked().getName())) {
                    PageInventory inventory = playersThatOpenedInventories.get(e.getWhoClicked().getName());
                    if (e.getCurrentItem().equals(next_page_item)) {
                        e.setCancelled(true);
                        inventory.openPage((Player) e.getWhoClicked(), Integer.valueOf(e.getClickedInventory().getTitle().replace(title + " - Seite #", "")).intValue() + 1);
                        return;
                    }
                    if (e.getCurrentItem().equals(last_page_item)) {
                        e.setCancelled(true);
                        open((Player) e.getWhoClicked());
                        return;
                    }
                    if (ItemManager.isCustomItem(e.getCurrentItem())) {
                        e.setCancelled(true);
                        e.getWhoClicked().getInventory().addItem(new LostItem(Item.valueOf(ItemManager.getNBTData(e.getCurrentItem(), "UUID"))).build(true));
                    }
                    Bukkit.getPluginManager().callEvent(new ExpandableInventoryClickEvent((Player) e.getWhoClicked(), playersThatOpenedInventories.get(e.getWhoClicked().getName()), e));
                }
            }

            @EventHandler
            public void on(InventoryCloseEvent e) {
                if (playersThatOpenedInventories.containsKey(e.getPlayer().getName())) {
                    playersThatOpenedInventories.remove(e.getPlayer().getName());
                }
            }
        };
        register();
        this.playersThatOpenedInventories = new HashMap<>();
        pages.add(new Page(title, size, pages.size()));
    }

    public void register() {
        Bukkit.getPluginManager().registerEvents(listener, plugin);
    }

    public void unregister() {
        HandlerList.unregisterAll(listener);
    }

    public void open(Player player) {
        openPage(player, 0);
    }

    public void openPage(Player player, int pageCount) {
        player.openInventory(pages.get(pageCount).getInventory());
        playersThatOpenedInventories.put(player.getName(), this);
    }

    public void addItem(ItemStack item) {
        pages.getLast().addItem(item);
        if (pages.getLast().isAboutToBeFull())

        {
            pages.getLast().getInventory().setItem(size - 1, next_page_item);
            pages.add(new Page(title, size, pages.size()));
        }

        if (pages.getLast().getCount() != 0) {
            pages.getLast().getInventory().setItem(size - 2, last_page_item);
        }
    }

    public static class ExpandableInventoryClickEvent extends Event implements Cancellable {
        private static final HandlerList HANDLERS = new HandlerList();
        private Player player;
        private PageInventory inventory;
        private InventoryClickEvent event;

        public ExpandableInventoryClickEvent(Player player, PageInventory inventory, InventoryClickEvent event) {
            this.player = player;
            this.inventory = inventory;
            this.event = event;
        }

        public Player getPlayer() {
            return player;
        }


        public PageInventory getInventory() {
            return inventory;
        }

        @Override
        public HandlerList getHandlers() {
            return HANDLERS;
        }


        public static HandlerList getHandlerList() {
            return HANDLERS;
        }

        @Override
        public boolean isCancelled() {
            return event.isCancelled();
        }

        @Override
        public void setCancelled(boolean arg0) {
            event.setCancelled(arg0);
        }
    }

    public static class Page {
        private Inventory inventory;
        private int count, itemCount;

        public Page(String title, int size, int count) {
            this.count = count;
            inventory = Bukkit.createInventory(null, size, title + " - Seite #" + count);
        }

        public void addItem(ItemStack item) {
            inventory.setItem(itemCount, item);
            itemCount++;
        }

        public boolean isAboutToBeFull() {
            return (inventory.getSize() - 3) == (itemCount - 1);
        }

        public int getItemCount() {
            return itemCount;
        }

        public Inventory getInventory() {
            return inventory;
        }

        public int getCount() {
            return count;
        }
    }
}