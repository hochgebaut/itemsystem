package de.dersnatch.lost.util;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import net.minecraft.server.v1_12_R1.NBTTagCompound;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Field;
import java.util.Base64;
import java.util.List;
import java.util.UUID;

public class LostItemBuilderAPI {

    private Material material;
    private ItemStack itemStack;
    private ItemMeta itemMeta;
    private String name;
    private List<String> lore;

    private static Class<?> skullMetaClass, tileEntityClass, blockPositionClass;
    private static int mcVersion;

    static {
        String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        mcVersion = Integer.parseInt(version.replaceAll("[^0-9]", ""));
        try {
            skullMetaClass = Class.forName("org.bukkit.craftbukkit." + version + ".inventory.CraftMetaSkull");
            tileEntityClass = Class.forName("net.minecraft.server." + version + ".TileEntitySkull");
            if (mcVersion > 174) {
                blockPositionClass = Class.forName("net.minecraft.server." + version + ".BlockPosition");
            } else {
                blockPositionClass = null;
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public LostItemBuilderAPI(Material material) {
        this.material = material;
        this.itemStack = new ItemStack(material);
    }

    public LostItemBuilderAPI(ItemStack item) {
        if (item.hasItemMeta()) {
            ItemMeta meta = item.getItemMeta();
            this.material = item.getType();
            if (meta.hasDisplayName()) {
                this.name = meta.getDisplayName();
            }
            if (meta.hasLore()) {
                this.lore = meta.getLore();
            }
        }
    }

    public LostItemBuilderAPI(Material material, short data) {
        this.itemStack = new ItemStack(material, 1, data);
    }

    public LostItemBuilderAPI(Material material, int amount, short data) {
        this.itemStack = new ItemStack(material, amount, data);
    }

    public LostItemBuilderAPI name(String name) {
        this.name = name;
        return this;
    }

    public LostItemBuilderAPI lore(List<String> lore) {
        this.lore = lore;
        return this;
    }

    public LostItemBuilderAPI setTexture(String texture) {
        String skinURL = convertToURL(texture);
        ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        SkullMeta meta = (SkullMeta) skull.getItemMeta();
        try {
            Field profileField = skullMetaClass.getDeclaredField("profile");
            profileField.setAccessible(true);
            profileField.set(meta, getProfile(skinURL));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        skull.setItemMeta(meta);
        this.itemStack = skull;
        return this;
    }

    public ItemStack getItemStack() {
        this.itemMeta = this.itemStack.getItemMeta();
        this.itemMeta.setDisplayName(this.name);
        this.itemMeta.setLore(this.lore);
        this.itemStack.setItemMeta(this.itemMeta);
        return this.itemStack;
    }

    public LostItemBuilderAPI clone() {
        LostItemBuilderAPI newBuilder = new LostItemBuilderAPI(this.material);
        newBuilder.name(this.name);
        newBuilder.lore(this.lore);
        return newBuilder;
    }

    public LostItemBuilderAPI addNBTData(String key, String value) {
        net.minecraft.server.v1_12_R1.ItemStack nmsCopy = CraftItemStack.asNMSCopy(this.itemStack);
        NBTTagCompound nbtTagCompound = (nmsCopy.hasTag()) ? nmsCopy.getTag() : new NBTTagCompound();
        nbtTagCompound.setString(key, value);
        nmsCopy.setTag(nbtTagCompound);
        this.itemStack = CraftItemStack.asBukkitCopy(nmsCopy);
        return this;
    }

    private static GameProfile getProfile(String skinURL) {
        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        String base64encoded = Base64.getEncoder().encodeToString(new String("{textures:{SKIN:{url:\"" + skinURL + "\"}}}").getBytes());
        Property property = new Property("textures", base64encoded);
        profile.getProperties().put("textures", property);
        return profile;
    }

    private String convertToURL(String rawInput) {
        byte[] input = Base64.getDecoder().decode(rawInput);
        String output = new String(input);
        String[] n = output.split("\"");
        for (String s : n) {
            if (s.startsWith("http://textures.minecraft.net/")) {
                return s;
            }
        }
        return null;
    }

    public LostItemBuilderAPI hideAttributes() {
        net.minecraft.server.v1_12_R1.ItemStack nmsCopy = CraftItemStack.asNMSCopy(this.itemStack);
        NBTTagCompound nbtTagCompound = (nmsCopy.hasTag()) ? nmsCopy.getTag() : new NBTTagCompound();
        nbtTagCompound.setInt("HideFlags", 126);
        nmsCopy.setTag(nbtTagCompound);
        this.itemStack = CraftItemStack.asBukkitCopy(nmsCopy);
        return this;
    }
}
